#include <fcntl.h>
#include <stdlib.h>

static const char s_hello[] = "Hello, ";
static const char s_world[] = "World!";

static void PrintAndExit()
{
    write(1, "Hello, world!\n", 14);
    exit(0);
}

const char* function_0()
{
    return "Hello, world!";
}

const char* function_1()
{
    return "Hello, world!";
}

const char* function_2()
{
    return "Hello, world!";
}

const char* function_3()
{
    return s_world;
}

const char* function_4()
{
    return "Hello, world!";
}

const char* function_5()
{
    return "Hello, world!";
}

const char* function_6()
{
    return "Hello, world!";
}

const char* function_7()
{
    return "Hello, world!";
}

const char* function_8()
{
    return s_hello;
}

const char* function_9()
{
    return "Hello, world!";
}

int setenv(const char *name, const char *value, int overwrite)
{
    return 0;
}

void _start()
{
    PrintAndExit();
}

void __libc_start_main()
{
    PrintAndExit();
}

void puts()
{
    PrintAndExit();
}

void __cxa_finalize()
{
    int a = 1;
}
