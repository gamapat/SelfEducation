#include <unistd.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

int main()
{
	if (fork() != 0)
	{
		_exec(0);
	}
	usleep(1000);
	char file[] = "/home/flag19/flag19";
	char* args[11] = {"/bin/sh", "-c", "/bin/getflag", NULL};
	char** env = {NULL};
	execve(file, args, env);
	printf("%d %s", errno, strerror(errno));
	return 0;
}