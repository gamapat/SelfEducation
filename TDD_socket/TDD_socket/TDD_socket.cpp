// TDD_socket.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "SocketWrapper.h"


int main(int argc, char** argv)
{
    bool isServer = false;
    if (argc == 2)
    {
        if (argv[1][0] == 's')
        {
            isServer = true;
        }
    }
    try
    {
        WSADATA wsaData;
        WSAStartup(MAKEWORD(2, 2), &wsaData);
        if (isServer)
        {
            SocketWrapper server;
            server.Bind("127.0.0.1", 4444);
            server.Listen();
            server.Accept();
        }
        else
        {
            SocketWrapper client;
            client.Connect("127.0.0.1", 4444);
        }
    }
    catch (const std::exception& e)
    {
        std::cerr << e.what();
    }
    system("pause");
    return 0;
}

