#include "stdafx.h"
#include "SocketWrapper.h"

namespace
{
    std::string GetExceptionString(const std::string& message, int errorCode)
    {
        std::stringstream stream;
        stream << message;
        stream << errorCode;
        stream << std::endl;
        return stream.str();
    }
}

SocketWrapper::SocketWrapper()
    : m_listenSocket(INVALID_SOCKET)
    , m_chatSocket(INVALID_SOCKET)
{
    m_listenSocket = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (m_listenSocket == INVALID_SOCKET)
    {
        throw std::runtime_error(GetExceptionString("Failed to create socket to listen on.", WSAGetLastError()));
    }
}

SocketWrapper::~SocketWrapper()
{
    closesocket(m_chatSocket);
    closesocket(m_listenSocket);
}

void SocketWrapper::Bind(const std::string& addr, int16_t port)
{
    sockaddr_in addres;
    addres.sin_family = AF_INET;
    addres.sin_addr.s_addr = inet_addr(addr.data());
    addres.sin_port = htons(port);
    if (bind(m_listenSocket, reinterpret_cast<sockaddr*>(&addres), sizeof(addres)) == SOCKET_ERROR)
    {
        throw std::runtime_error(GetExceptionString("Failed to bind socket to address.", WSAGetLastError()));
    }
}

void SocketWrapper::Listen()
{
    if (listen(m_listenSocket, SOMAXCONN) == SOCKET_ERROR)
    {
        throw std::runtime_error(GetExceptionString("Failed to listen on socket.", WSAGetLastError()));
    }
}

void SocketWrapper::Accept()
{
    m_chatSocket = accept(m_listenSocket, nullptr, nullptr);
    if (m_chatSocket == INVALID_SOCKET)
    {
        throw std::runtime_error(GetExceptionString("Failed to connect to client.", WSAGetLastError()));
    }
}

void SocketWrapper::Connect(const std::string& addr, int16_t port)
{
    sockaddr_in addres;
    addres.sin_family = AF_INET;
    addres.sin_addr.s_addr = inet_addr(addr.data());
    addres.sin_port = htons(port);
    m_chatSocket = connect(m_listenSocket, reinterpret_cast<sockaddr*>(&addres), sizeof(addres));
    if (m_chatSocket == INVALID_SOCKET)
    {
        throw std::runtime_error(GetExceptionString("Failed to connect to server.", WSAGetLastError()));
    }
}

void SocketWrapper::Read(std::vector<char>& buffer)
{
    recv(m_chatSocket, buffer.data(), buffer.size(), 0);
}

void SocketWrapper::Write(const std::vector<char>& buffer)
{
    send(m_chatSocket, buffer.data(), buffer.size(), 0);
}
