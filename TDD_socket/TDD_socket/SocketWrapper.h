#pragma once
class SocketWrapper
{
public:
    SocketWrapper();
    ~SocketWrapper();

    void Bind(const std::string& addr, int16_t port);
    void Listen();
    void Accept();
    void Connect(const std::string& addr, int16_t port);
    void Read(std::vector<char>& buffer);
    void Write(const std::vector<char>& buffer);

private:
    SOCKET m_listenSocket;
    SOCKET m_chatSocket;
};

