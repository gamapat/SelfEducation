// OpenSslSecond.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main()
{
    std::string strLine;
    std::vector<char> charLine(10000, 0);
    const size_t blockSize = 16;
    for (size_t i = 0; (std::cin >> charLine.data()).good(); ++i)
    {
        strLine = std::string(charLine.data());
        for (size_t leftBlock = 0; leftBlock < strLine.size(); leftBlock += blockSize)
        {
            std::string leftBlockStr = strLine.substr(leftBlock, blockSize);
            for (size_t rightBlock = 0; rightBlock < strLine.size(); rightBlock += blockSize)
            {
                if (leftBlock != rightBlock)
                {
                    std::string rightBlockStr = strLine.substr(rightBlock, blockSize);
                    if (leftBlockStr == rightBlockStr)
                    {
                        std::cout << "Found ECB line. It's number is " << i << std::endl;
                        return 0;
                    }
                }
            }
        }
        std::vector<char>(10000, 0).swap(charLine);
    }
    std::cout << "Did not found ECB line." << std::endl;
    return -1;
}

