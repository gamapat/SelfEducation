// OpenSslFirst.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

static std::vector<uint8_t> s_key = { 'm', 'y', '_', 'k', 'e', 'y', '_', 'i', 's', '_', '2', '5', '6', '_', 'b', 'i', 't', 's', '_', 'l', 'o', 'n', 'g', '_', '_', '_', '_', '_', '_', '_', '_', '_'};
static std::vector<uint8_t> s_iv = { 'm', 'y', '_', 'i', 'v', '_', 'm', 'y', '_', 'i', 'v', '_', 'm', 'y', '_', 'i' };

int main(int argc, const char** argv)
{
    if (argc != 2)
    {
        std::cerr << "Wrong usage. Give the one string to encrypt it.";
        return -1;
    }

    const std::string stringToEncrypt(argv[1]);
    std::vector<uint8_t> encryptedData(stringToEncrypt.length());
    int encrLen = encryptedData.size();
    EVP_CIPHER_CTX *encrCtx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(encrCtx);
    EVP_EncryptInit_ex(encrCtx, EVP_aes_256_cbc(), NULL, s_key.data(), s_iv.data());
    EVP_EncryptUpdate(encrCtx, encryptedData.data(), &encrLen, reinterpret_cast<const unsigned char*>(stringToEncrypt.data()), stringToEncrypt.length());
    EVP_EncryptFinal(encrCtx, encryptedData.data() + encrLen, &encrLen);
    EVP_CIPHER_CTX_free(encrCtx);

    BIO *bio, *b64;
    BUF_MEM *bufferPtr;

    b64 = BIO_new(BIO_f_base64());
    bio = BIO_new(BIO_s_mem());
    bio = BIO_push(b64, bio);

    BIO_set_flags(bio, BIO_FLAGS_BASE64_NO_NL); //Ignore newlines - write everything in one line
    BIO_write(bio, encryptedData.data(), encryptedData.size());
    BIO_flush(bio);
    BIO_get_mem_ptr(bio, &bufferPtr);
    BIO_set_close(bio, BIO_NOCLOSE);
    BIO_free_all(bio);

    std::string finalData(bufferPtr->data);
    std::cout << finalData;

    return 0;
}

