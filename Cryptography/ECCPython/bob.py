import ecc
import binascii

bob_private_key, bob_public_key = ecc.make_keypair()

print("Bob's private key:", hex(bob_private_key))
print("Bob's public key: (0x{:x}, 0x{:x})".format(*bob_public_key))

alice_public_key_x = int(input(), 16)
alice_public_key_y = int(input(), 16)
alice_public_key = (alice_public_key_x, alice_public_key_y)

session_key = ecc.scalar_mult(bob_private_key, alice_public_key)
print('Shared secret: (0x{:x}, 0x{:x})'.format(*session_key))

key = ecc.get_encryption_key(session_key[0])
print('Encryption key: {}'.format(key))

message = input()
encrypted = binascii.unhexlify(message)
decrypted = ecc.decrypt(encrypted, key)
print("Message: {}".format(encrypted))
print("Decrypted message: {}".format(decrypted))

encrypted = ecc.encrypt(decrypted, key)
print("Encrypted message: {}".format(binascii.hexlify(encrypted)))