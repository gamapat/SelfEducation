import ecc
import binascii

alice_private_key, alice_public_key = ecc.make_keypair()

print("Alice's private key:", hex(alice_private_key))
print("Alice's public key: (0x{:x}, 0x{:x})".format(*alice_public_key))

bob_public_key_x = int(input(), 16)
bob_public_key_y = int(input(), 16)
bob_public_key = (bob_public_key_x, bob_public_key_y)

session_key = ecc.scalar_mult(alice_private_key, bob_public_key)
print('Shared secret: (0x{:x}, 0x{:x})'.format(*session_key))

key = ecc.get_encryption_key(session_key[0])
print('Encryption key: {}'.format(key))

message =  "An EC Parameters file contains all of the information necessary to define an Elliptic Curve that can then be used for cryptographic operations (for OpenSSL this means ECDH and ECDSA)."
encrypted = ecc.encrypt(message.encode("utf-8"), key)
print("Message: {}".format(message))
print("Encrypted message: {}".format(binascii.hexlify(encrypted)))

encrypted = binascii.unhexlify(input())
decrypted = ecc.decrypt(encrypted, key)
print("Decrypted message: {}".format(decrypted))