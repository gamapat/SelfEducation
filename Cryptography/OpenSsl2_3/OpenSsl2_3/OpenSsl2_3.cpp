// OpenSsl2_3.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


int main(int argc, const char** argv)
{
    if (argc != 3)
    {
        std::cout << "Usage is \n\t" << "<a> <p>" << std::endl;
    }
    auto a = std::strtoul(argv[1], nullptr, 0);
    auto p = std::strtoul(argv[2], nullptr, 0);
    for (size_t i = 1; i < p; ++i)
    {
        if ((i * a) % p == 1)
        {
            std::cout << "Inverted value for " << a << " is " << i << std::endl;
            return 0;
        }
    }
    std::cout << "Inverted value was not found" << std::endl;
    return 0;
}

