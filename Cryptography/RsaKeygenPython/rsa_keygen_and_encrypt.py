from random import randrange, getrandbits
import binascii

def miller_rabin(n, k=10):
	if n == 2:
		return True
	if not n & 1:
		return False

	def check(a, s, d, n):
		x = pow(a, d, n)
		if x == 1:
			return True
		for i in range(s - 1):
			if x == n - 1:
				return True
			x = pow(x, 2, n)
		return x == n - 1

	s = 0
	d = n - 1

	while d % 2 == 0:
		d >>= 1
		s += 1

	for i in range(k):
		a = randrange(2, n - 1)
		if not check(a, s, d, n):
			return False
	return True

def gen_key(bit_number):
	while True:
		number = getrandbits(bit_number)
		if miller_rabin(number, 512):
			return number

def egcd(a, b):
    s1, s2 = 1, 0   
    t1, t2 = 0, 1   
    while b!=0:   
        q = a//b    
        r = a%b   
        a, b = b, r     
        s = s1-q*s2    
        s1, s2 = s2, s      
        t = t1-q*t2    
        t1, t2 = t2, t    
    return (s1, t1)

def modpow(base, exp, modulus):
	base %= modulus
	result = 1
	while exp > 0:
		if exp & 1:
		    result = (result * base) % modulus
		base = (base * base) % modulus
		exp = exp // 2
	return result

def encrypt_chunk(text, result_size, key, module):
	data = text.encode("utf-8")
	data_int = int(data.hex(), 16)
	result = modpow(data_int, key, module)
	hexed = hex(result)[2:]
	if len(hexed) % 2 == 1:
		hexed = "0" + hexed
	result_bytes = bytes(binascii.unhexlify(hexed))
	if len(result_bytes) < result_size:
		result_bytes = result_bytes + ("\x00" * (result_size - len(result_bytes)))
	return result_bytes

def encrypt(text, key, module, chunk_size):
	chuncks = [text[i: i+chunk_size] for i in range(0, len(text), chunk_size - 1)]
	encr_data = b""
	for chunk in chuncks:
		encr_data = encr_data + encrypt_chunk(chunk, chunk_size, key, module)
	return encr_data

def decrypt_chunk(text, result_size, key, module):
	data = text
	data_int = int(data.hex(), 16)
	result = modpow(data_int, key, module)
	hexed = hex(result)[2:]
	if len(hexed) % 2 == 1:
		hexed = "0" + hexed
	result_bytes = bytes(binascii.unhexlify(hexed))
	return result_bytes

def decrypt(text, key, module, chunk_size):
	chuncks = [text[i: i+chunk_size] for i in range(0, len(text), chunk_size)]
	decr_data = b""
	for chunk in chuncks:
		decr_data = decr_data + decrypt_chunk(chunk, chunk_size - 1, key, module)
	return decr_data

def do_rsa(bit_number):
	e = 17
	while True:
		p = gen_key(bit_number // 2)
		q = gen_key(bit_number // 2)
		n = p * q
		fi = (p - 1) * (q - 1)
		if fi % e != 0:
			break
		print("Failed try to create key pair")
	d, y = egcd(e, fi)
	if d < 0:
		d += fi
	print ("p: {}\nq: {}\nn: {}\nfi: {}\ne: {}\nd: {}".format(hex(p), hex(q), hex(n), hex(fi), hex(e), hex(d)))
	magic = "THE MAGIC WORDS ARE SQUEAMISH OSSIFRAGE"
	chunk_size = bit_number//8
	encr_data = encrypt(magic, e, n, chunk_size)
	decr_data = decrypt(encr_data, d, n, chunk_size)
	with open("encrypted", "wb") as encr_file:
		encr_file.write(encr_data)
	with open("decrypted", "wb") as decr_file:
		decr_file.write(decr_data)
do_rsa(1024)