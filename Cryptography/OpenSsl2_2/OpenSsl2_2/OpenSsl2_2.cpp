// OpenSsl2_2.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

namespace
{
    const uint8_t s_toEncrypt[] = "prev_val";
}

int main()
{   
    BIO* bio = BIO_new_file("private_key.pem", "r");
    RSA* myRSA = PEM_read_bio_RSAPrivateKey(bio, nullptr, 0, "");
    std::vector<uint8_t> signedData(RSA_size(myRSA));
    RSA_private_encrypt(sizeof(s_toEncrypt), s_toEncrypt, signedData.data(), myRSA, RSA_PKCS1_PADDING);
    EVP_MD_CTX* ctx = EVP_MD_CTX_new();
    EVP_DigestInit_ex(ctx, EVP_md5(), nullptr);
    EVP_DigestUpdate(ctx, signedData.data(), signedData.size());
    std::vector<uint8_t> mdValue(EVP_MAX_MD_SIZE);
    uint32_t size = 0;
    EVP_DigestFinal_ex(ctx, mdValue.data(), &size);

    std::string result = BN_bn2hex(BN_bin2bn(mdValue.data(), mdValue.size(), nullptr));
    std::cout << result << std::endl;
    return 0;
}

