// RsaKeygen.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

using DataVector = std::vector<uint8_t>;

namespace
{
    const size_t s_e = 17;
    const size_t s_rawChunkSizeInBytes = 3;
    const size_t s_encryptedChankSizeInBytes = 4;
    const std::string s_message = "THE MAGIC WORDS ARE SQUEAMISH OSSIFRAGE";

    uint64_t getMultiInverted(uint64_t fi, uint64_t e, int64_t & x, int64_t & y)
    {
        if (fi == 0)
        {
            x = 0; y = 1;
            return e;
        }
        int64_t x1, y1;
        uint64_t d = getMultiInverted(e%fi, fi, x1, y1);
        x = y1 - (e / fi) * x1;
        y = x1;
        return d;
    }

    template <typename T>
    T modpow(T base, T exp, T modulus) {
        base %= modulus;
        T result = 1;
        while (exp > 0) {
            if (exp & 1)
            {
                result = (result * base) % modulus;
            }
            base = (base * base) % modulus;
            exp >>= 1;
        }
        return result;
    }

    DataVector EncryptChunk(const DataVector& inputData, uint64_t key, uint64_t module)
    {
        DataVector ret(s_encryptedChankSizeInBytes);
        uint64_t inputDataValue = 0;
        memcpy(&inputDataValue, inputData.data(), inputData.size());
        uint64_t encryptedValue = modpow(inputDataValue, key, module);
        memcpy(ret.data(), &encryptedValue, ret.size());
        return ret;
    }

    DataVector DecryptChunk(const DataVector& inputData, uint64_t key, uint64_t module)
    {
        DataVector ret(s_rawChunkSizeInBytes);
        uint64_t inputDataValue = 0;
        memcpy(&inputDataValue, inputData.data(), inputData.size());
        uint64_t encryptedValue = modpow(inputDataValue, key, module);
        memcpy(ret.data(), &encryptedValue, ret.size());
        return ret;
    }

    DataVector EncryptText(const std::string& text, uint64_t key, uint64_t module)
    {
        DataVector ret;
        for (size_t i = 0; i < text.size(); i += s_rawChunkSizeInBytes)
        {
            DataVector chunk(text.begin() + i, text.begin() + i + s_rawChunkSizeInBytes);
            DataVector encryptedChunk = EncryptChunk(chunk, key, module);
            ret.insert(ret.end(), encryptedChunk.begin(), encryptedChunk.end());
        }
        return ret;
    }

    std::string DecyptText(const DataVector& text, uint64_t key, uint64_t module)
    {
        DataVector retBytes;
        for (size_t i = 0; i < text.size(); i += s_encryptedChankSizeInBytes)
        {
            DataVector chunk(text.begin() + i, text.begin() + i + s_encryptedChankSizeInBytes);
            DataVector encryptedChunk = DecryptChunk(chunk, key, module);
            retBytes.insert(retBytes.end(), encryptedChunk.begin(), encryptedChunk.end());
        }
        std::string ret(retBytes.begin(), retBytes.end());
        return ret;
    }
}

int main()
{
    uint32_t p = 56873; // values was taken from https://asecuritysite.com/encryption/random3?val=16
    uint32_t q = 36683;
    std::cout << "p is : " << std::hex << p << std::endl;
    std::cout << "q is : " << std::hex << q << std::endl;
    uint64_t n = static_cast<uint64_t>(p) * static_cast<uint64_t>(q);
    std::cout << "n is : " << std::hex << n << std::endl;
    uint64_t fi = (static_cast<uint64_t>(p) - 1) * (static_cast<uint64_t>(q) - 1);
    std::cout << "fi(n) is : " << std::hex << fi << std::endl;
    int64_t x = 0;
    int64_t y = 0;
    if (getMultiInverted(s_e, fi, x, y) != 1)
    {
        std::cout << "GCD is greater than 1. Find enother exponent or p and q";
        return -1;
    }
    if (x < 0)
    {
        x += fi;
    }
    uint64_t d = x;
    std::cout << "e is : " << std::hex << s_e << std::endl;
    std::cout << "d is : " << std::hex << d << std::endl;
    auto encrypted = EncryptText(s_message, s_e, n);
    std::ofstream enc("encrypted", std::ios::binary);
    enc.write(reinterpret_cast<char*>(encrypted.data()), encrypted.size());
    auto decrypted = DecyptText(encrypted, d, n);
    std::ofstream dec("decrypted", std::ios::binary);
    dec.write(decrypted.data(), decrypted.size());
    return 0;
}

