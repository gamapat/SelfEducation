// ECC_C++.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
namespace
{
    const std::string s_message = "An EC Parameters file contains all of the information necessary to define an Elliptic Curve that can then be used for cryptographic operations (for OpenSSL this means ECDH and ECDSA).";
    static void *KDF1_SHA256(const void *in, size_t inlen, void *out, size_t *outlen)
    {
        if (*outlen < SHA256_DIGEST_LENGTH)
            return NULL;
        else
            *outlen = SHA256_DIGEST_LENGTH;
        return SHA256(reinterpret_cast<const unsigned char*>(in), inlen, reinterpret_cast<unsigned char*>(out));
    }

    void handleErrors()
    {
        ERR_print_errors_fp(stdout);
    }

    EC_GROUP *create_curve(void)
    {
        BN_CTX *ctx;
        EC_GROUP *curve;
        BIGNUM *a, *b, *p, *order, *x, *y;
        EC_POINT *generator;

        /* Binary data for the curve parameters */
        unsigned char a_bin[] = { 
            0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
            0xff, 0xfc };
        unsigned char b_bin[] = { 
            0x51, 0x95, 0x3e, 0xb9, 0x61, 0x8e, 0x1c, 0x9a,
            0x1f, 0x92, 0x9a, 0x21, 0xa0, 0xb6, 0x85, 0x40,
            0xee, 0xa2, 0xda, 0x72, 0x5b, 0x99, 0xb3, 0x15,
            0xf3, 0xb8, 0xb4, 0x89, 0x91, 0x8e, 0xf1, 0x09,
            0xe1, 0x56, 0x19, 0x39, 0x51, 0xec, 0x7e, 0x93,
            0x7b, 0x16, 0x52, 0xc0, 0xbd, 0x3b, 0xb1, 0xbf, 
            0x07, 0x35, 0x73, 0xdf, 0x88, 0x3d, 0x2c, 0x34, 
            0xf1, 0xef, 0x45, 0x1f, 0xd4, 0x6b, 0x50, 0x3f, 
            0x00 };
        unsigned char p_bin[] = { 
            0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 
            0xff, 0xff };
        unsigned char order_bin[] = { 
            0x01, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff, 0xff,
            0xff, 0xfa, 0x51, 0x86, 0x87, 0x83, 0xbf, 0x2f,
            0x96, 0x6b, 0x7f, 0xcc, 0x01, 0x48, 0xf7, 0x09,
            0xa5, 0xd0, 0x3b, 0xb5, 0xc9, 0xb8, 0x89, 0x9c,
            0x47, 0xae, 0xbb, 0x6f, 0xb7, 0x1e, 0x91, 0x38,
            0x64, 0x09 };
        unsigned char x_bin[] = { 
            0x00, 0xc6, 0x85, 0x8e, 0x06, 0xb7, 0x04, 0x04,
            0xe9, 0xcd, 0x9e, 0x3e, 0xcb, 0x66, 0x23, 0x95,
            0xb4, 0x42, 0x9c, 0x64, 0x81, 0x39, 0x05, 0x3f,
            0xb5, 0x21, 0xf8, 0x28, 0xaf, 0x60, 0x6b, 0x4d,
            0x3d, 0xba, 0xa1, 0x4b, 0x5e, 0x77, 0xef, 0xe7,
            0x59, 0x28, 0xfe, 0x1d, 0xc1, 0x27, 0xa2, 0xff,
            0xa8, 0xde, 0x33, 0x48, 0xb3, 0xc1, 0x85, 0x6a,
            0x42, 0x9b, 0xf9, 0x7e, 0x7e, 0x31, 0xc2, 0xe5, 
            0xbd, 0x66 };
        unsigned char y_bin[] = { 
            0x01, 0x18, 0x39, 0x29, 0x6a, 0x78, 0x9a, 0x3b,
            0xc0, 0x04, 0x5c, 0x8a, 0x5f, 0xb4, 0x2c, 0x7d,
            0x1b, 0xd9, 0x98, 0xf5, 0x44, 0x49, 0x57, 0x9b,
            0x44, 0x68, 0x17, 0xaf, 0xbd, 0x17, 0x27, 0x3e,
            0x66, 0x2c, 0x97, 0xee, 0x72, 0x99, 0x5e, 0xf4,
            0x26, 0x40, 0xc5, 0x50, 0xb9, 0x01, 0x3f, 0xad, 
            0x07, 0x61, 0x35, 0x3c, 0x70, 0x86, 0xa2, 0x72, 
            0xc2, 0x40, 0x88, 0xbe, 0x94, 0x76, 0x9f, 0xd1, 
            0x66, 0x50 };

        /* Set up the BN_CTX */
        if (NULL == (ctx = BN_CTX_new())) handleErrors();

        /* Set the values for the various parameters */
        if (NULL == (a = BN_bin2bn(a_bin, sizeof(a_bin), NULL))) handleErrors();
        if (NULL == (b = BN_bin2bn(b_bin, sizeof(b_bin), NULL))) handleErrors();
        if (NULL == (p = BN_bin2bn(p_bin, sizeof(p_bin), NULL))) handleErrors();
        if (NULL == (order = BN_bin2bn(order_bin, sizeof(order_bin), NULL))) handleErrors();
        if (NULL == (x = BN_bin2bn(x_bin, sizeof(x_bin), NULL))) handleErrors();
        if (NULL == (y = BN_bin2bn(y_bin, sizeof(y_bin), NULL))) handleErrors();

        /* Create the curve */
        if (NULL == (curve = EC_GROUP_new_curve_GFp(p, a, b, ctx))) handleErrors();

        /* Create the generator */
        if (NULL == (generator = EC_POINT_new(curve))) handleErrors();
        if (1 != EC_POINT_set_affine_coordinates_GFp(curve, generator, x, y, ctx))
            handleErrors();

        /* Set the generator and the order */
        if (1 != EC_GROUP_set_generator(curve, generator, order, NULL))
            handleErrors();

        EC_POINT_free(generator);
        BN_free(y);
        BN_free(x);
        BN_free(order);
        BN_free(p);
        BN_free(b);
        BN_free(a);
        BN_CTX_free(ctx);

        return curve;
    }
}

int main()
{
    auto alice_key = EC_KEY_new();
    auto bob_key = EC_KEY_new();
    auto curve = create_curve();
    EC_KEY_set_group(alice_key, curve);
    EC_KEY_set_group(bob_key, curve);
    EC_KEY_generate_key(alice_key);
    EC_KEY_generate_key(bob_key);

    EC_KEY_print_fp(stdout, alice_key, 0);
    EC_KEY_print_fp(stdout, bob_key, 0);

    size_t secretLength = (EC_GROUP_get_degree(curve) + 7) / 8;
    EC_GROUP_free(curve);
    std::vector<uint8_t> aliceSecret(SHA256_DIGEST_LENGTH, 0);
    ECDH_compute_key(aliceSecret.data(), secretLength, EC_KEY_get0_public_key(bob_key), alice_key, KDF1_SHA256);
    std::vector<uint8_t> bobSecret(SHA256_DIGEST_LENGTH, 0);
    ECDH_compute_key(bobSecret.data(), secretLength, EC_KEY_get0_public_key(alice_key), bob_key, KDF1_SHA256);
    auto aliceSecretBn = BN_bin2bn(aliceSecret.data(), SHA256_DIGEST_LENGTH, nullptr);
    std::cout << "Alice shared key" << std::endl;
    BN_print_fp(stdout, aliceSecretBn);
    auto bobSecretBn = BN_bin2bn(bobSecret.data(), SHA256_DIGEST_LENGTH, nullptr);
    std::cout << std::endl << "Bob shared key" << std::endl;
    BN_print_fp(stdout, bobSecretBn);
    BN_free(aliceSecretBn);
    BN_free(bobSecretBn);
    
    EVP_CIPHER_CTX* ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(ctx);
    EVP_CIPHER_CTX_set_padding(ctx, false);
    EVP_EncryptInit_ex(ctx, EVP_aes_256_ecb(), nullptr, aliceSecret.data(), nullptr);
    std::vector<uint8_t> encryptedData(s_message.size() + SHA256_DIGEST_LENGTH, 0);
    int encrLength = 0;
    EVP_EncryptUpdate(ctx, encryptedData.data(), &encrLength, reinterpret_cast<const unsigned char*>(s_message.data()), s_message.size());
    int encFinalLength = encrLength;
    EVP_EncryptFinal_ex(ctx, encryptedData.data() + encrLength, &encrLength);
    encFinalLength += encrLength;
    EVP_CIPHER_CTX_free(ctx);

    auto encryptedMessageBn = BN_bin2bn(encryptedData.data(), encFinalLength, nullptr);
    std::cout << std::endl << "Encrypted message" << std::endl;
    BN_print_fp(stdout, encryptedMessageBn);
    BN_free(encryptedMessageBn);

    ctx = EVP_CIPHER_CTX_new();
    EVP_CIPHER_CTX_init(ctx);
    EVP_CIPHER_CTX_set_padding(ctx, false);
    EVP_DecryptInit_ex(ctx, EVP_aes_256_ecb(), nullptr, bobSecret.data(), nullptr);
    std::vector<uint8_t> decryptedData(encFinalLength + SHA256_DIGEST_LENGTH, 0);
    int decryptLength = 0;
    EVP_DecryptUpdate(ctx, decryptedData.data(), &decryptLength, encryptedData.data(), encFinalLength);
    auto decryptFinalLength = decryptLength;
    EVP_DecryptFinal_ex(ctx, decryptedData.data() + decryptLength, &decryptLength);
    decryptFinalLength += decryptLength;
    EVP_CIPHER_CTX_free(ctx);

    std::string result(decryptedData.begin(), decryptedData.begin() + decryptFinalLength);

    std::cout << std::endl << "Decrypted message" << std::endl << result;

    return 0;
}

