// STL.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

const size_t s_insertElementsCount = 1000;
const size_t s_insertMoreElementsCount = 1000000;

void insertInMap(size_t count, std::map<int, int>& map)
{
    for (int i = 0; i < count; ++i)
    {
        map[i] = i;
    }
}

void insertInset(size_t count, std::set<int>& set)
{
    for (int i = 0; i < count; ++i)
    {
        set.insert(set.end(), i);
    }
}

void insertInVector(size_t count, std::vector<int>& vec)
{
    for (int i = 0; i < count; ++i)
    {
        vec.push_back(i);
    }
}

void insertLargerInMap(size_t count, std::map<int, int>& map)
{
    for (int i = 0; i < count; ++i)
    {
        map[i] = i;
    }
}

void insertLargerInset(size_t count, std::set<int>& set)
{
    for (int i = 0; i < count; ++i)
    {
        set.insert(set.end(), i);
    }
}

void insertLargerInVector(size_t count, std::vector<int>& vec)
{
    for (int i = 0; i < count; ++i)
    {
        vec.push_back(i);
    }
}

void removeFromMap(size_t count, std::map<int, int>& map)
{
    for (int i = 0; i < count; ++i)
    {
        map.erase(i);
    }
}

void removeFromset(size_t count, std::set<int>& set)
{
    for (int i = 0; i < count; ++i)
    {
        set.erase(i);
    }
}

void removeFromVector(size_t count, std::vector<int>& vec)
{
    for (int i = 0; i < count; ++i)
    {
        vec.erase(vec.begin());
    }
}

int main()
{
    std::string fileNameBase = "TimeMetrics";
    for (int i = 0; i < 10; ++i)
    {
        std::map<int, int> myMap{};
        std::set<int> myset{};
        std::vector<int> myVector{};

        std::stringstream strBuilder;
        strBuilder << fileNameBase;
        strBuilder << i + 1;
        std::string filename = strBuilder.str();
        std::ofstream out(filename, std::ios_base::out);
        if (!out.good())
        {
            std::cerr << "Failed to create file " << filename.c_str() << std::endl;
            break;
        }
        auto start_insertInMap_time = std::chrono::steady_clock::now();
        insertInMap(s_insertElementsCount, myMap);
        auto stop_insertInMap_time = std::chrono::steady_clock::now();
        insertInset(s_insertElementsCount, myset);
        auto stop_insertInset_time = std::chrono::steady_clock::now();
        insertInVector(s_insertElementsCount, myVector);
        auto stop_insertInVector_time = std::chrono::steady_clock::now();

        auto start_removeFromMap_time = std::chrono::steady_clock::now();
        removeFromMap(s_insertElementsCount, myMap);
        auto start_removeFromset_time = std::chrono::steady_clock::now();
        removeFromset(s_insertElementsCount, myset);
        auto start_removeFromVector_time = std::chrono::steady_clock::now();
        removeFromVector(s_insertElementsCount, myVector);
        auto stop_removeFromVector_time = std::chrono::steady_clock::now();

        insertLargerInMap(s_insertMoreElementsCount, myMap);
        insertLargerInset(s_insertMoreElementsCount, myset);
        insertLargerInVector(s_insertMoreElementsCount, myVector);
        auto start_findLastInMap_time = std::chrono::steady_clock::now();
        myMap.find(s_insertMoreElementsCount - 1);
        auto start_findLastInset_time = std::chrono::steady_clock::now();
        myset.find(s_insertMoreElementsCount - 1);
        auto start_findLastInVector_time = std::chrono::steady_clock::now();
        std::find(myVector.begin(), myVector.end(), s_insertMoreElementsCount - 1);
        auto stop_findLastInVector_time = std::chrono::steady_clock::now();
        out << "InsertInMap: " << std::chrono::duration_cast<std::chrono::nanoseconds>(stop_insertInMap_time - start_insertInMap_time).count() << std::endl;
        out << "InsertInset: " << std::chrono::duration_cast<std::chrono::nanoseconds>(stop_insertInset_time - stop_insertInMap_time).count() << std::endl;
        out << "InsertInVector: " << std::chrono::duration_cast<std::chrono::nanoseconds>(stop_insertInVector_time - stop_insertInset_time).count() << std::endl;

        out << "RemoveFromMap: " << std::chrono::duration_cast<std::chrono::nanoseconds>(start_removeFromset_time - start_removeFromMap_time).count() << std::endl;
        out << "RemoveFromset: " << std::chrono::duration_cast<std::chrono::nanoseconds>(start_removeFromVector_time - start_removeFromset_time).count() << std::endl;
        out << "RemoveFromVector: " << std::chrono::duration_cast<std::chrono::nanoseconds>(stop_removeFromVector_time - start_removeFromVector_time).count() << std::endl;

        out << "FindLastInMap: " << std::chrono::duration_cast<std::chrono::nanoseconds>(start_findLastInset_time - start_findLastInMap_time).count() << std::endl;
        out << "FindLastInset: " << std::chrono::duration_cast<std::chrono::nanoseconds>(start_findLastInVector_time - start_findLastInset_time).count() << std::endl;
        out << "FindLastInVector: " << std::chrono::duration_cast<std::chrono::nanoseconds>(stop_findLastInVector_time - start_findLastInVector_time).count() << std::endl;
    }

    return 0;
}

