#include <iostream>

int main() {
    char cryptedToken[36];
    std::cin >> cryptedToken;
    char key = 'w' ^ '-';
    for (int i = 0; i < strlen(cryptedToken); ++i)
    {
	    std::cout << (char)(cryptedToken[i] ^ key);
    }
    return 0;
}