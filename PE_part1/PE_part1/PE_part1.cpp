// PE_part1.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <stdio.h>
#include <errno.h>
#include <string>

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        std::cout << "Usage is:" << std::endl;
        std::cout << argv[0] << " D:/path/to/executable" << std::endl;
        return 1;
    }
    char* fileName = argv[1];
    auto file = std::fopen(fileName, "r");
    if (file == nullptr)
    {
        std::cout << "[ERROR] Failed to open file " << fileName << std::endl;
        std::cout << "[ERROR] Code is " << errno << std::endl;
        return 1;
    }

    IMAGE_DOS_HEADER header;
    std::fread(&header, sizeof(header), 1, file);
    if ((header.e_magic & 0xFFFF) != 0x5A4D)
    {
        std::cout << "[ERROR] File is not executable " << fileName << std::endl;
        return 1;
    }
    std::cout << "[DEBUG] Offset of PE header: 0x" << std::hex << header.e_lfanew << std::endl;
    std::fseek(file, header.e_lfanew, SEEK_SET);
    IMAGE_NT_HEADERS32 peHeader;
    std::fread(&peHeader, sizeof(peHeader), 1, file);
    if (peHeader.Signature & 0xFFFF != 0x4550)
    {
        std::cout << "[ERROR] Didn't recognize PE header magic" << fileName << std::endl;
        return 1;
    }
    std::cout << "[DEBUG] Found " << peHeader.FileHeader.NumberOfSections << " sections" << std::endl;
    for (int i = 0; i < peHeader.FileHeader.NumberOfSections; ++i)
    {
        IMAGE_SECTION_HEADER sectionHeader;
        std::fread(&sectionHeader, sizeof(sectionHeader), 1, file);
        std::cout << "[ANSWER] Section #" << i + 1
            << ". Name is " << sectionHeader.Name
            << " , size is 0x" << std::hex << sectionHeader.SizeOfRawData
            << " , offset is 0x" << std::hex << sectionHeader.PointerToRawData << std::endl;
    }
    return 0;
}

